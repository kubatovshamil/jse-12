# TASK MANAGER

## DEVELOPER INFO

**NAME:** Shamil Kubatov

**E-MAIL:** shamil.kubatov@mail.ru

## SYSTEM INFO

**OS**: Windows 10 1809

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: i5

## BUILD PROJECT

```
mvn clean install
```

## RUN PROJECT
```
cd ./target
java -jar ./task-manager.jar
```